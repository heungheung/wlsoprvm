#!/bin/bash

#usage:
# bash <(curl -s https://gitlab.com/heungheung/wlsoprvm/-/raw/master/setup-operator-workshop.sh)
# bash <(curl -s https://raw.githubusercontent.com/nagypeter/vmcontrol/master/setup-operator-workshop.sh)
echo
echo "This scripts is to setup VM enviroment for WLS Opr Workshop"
echo "Tested with CentOS 7"
echo
echo "Setup environment for WebLogic Kubernetes Operator Workshop..."

#clean
# remove fonts to save disk space
echo "entering script to clean up VM"
echo "=================================================="
curl -LSs https://gitlab.com/heungheung/wlsoprvm/-/raw/master/scripts/clean-vm.sh | bash
# stop autoupdate
echo "entering script to config VM and install python3"
echo "=================================================="
curl -LSs https://gitlab.com/heungheung/wlsoprvm/-/raw/master/scripts/config-vm.sh | bash
# install oci cli
echo "entering script to intall OCI CLI"
echo "=================================================="
curl -LSs https://gitlab.com/heungheung/wlsoprvm/-/raw/master/scripts/install-oci.sh | bash
# install kubectl and helm
echo "entering script to install kubectl and helm"
echo "=================================================="
curl -LSs https://gitlab.com/heungheung/wlsoprvm/-/raw/master/scripts/install-kubectl.sh | bash
# install git client cli
echo "entering script to install git client"
echo "=================================================="
curl -LSs https://gitlab.com/heungheung/wlsoprvm/-/raw/master/scripts/install-git.sh | bash
# clone the wlsopr sample repo
echo "entering script ot clone git repo"
echo "=================================================="
curl -LSs https://gitlab.com/heungheung/wlsoprvm/-/raw/master/scripts/clone-weblogic-kubernetes-operator.sh | bash

echo "Setup is complete for WebLogic Kubernetes Operator Workshop."

echo "Available disk size: "
df -h | awk '$NF == "/" { print $4 }'
